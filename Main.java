package nodomain.areas;

import com.google.gson.*;
import com.google.gson.stream.JsonReader;

import java.io.*;
import java.util.ArrayList;


public class Main {

    public static void main(String[] args) {
	System.out.println("Hi");
	AreaCreator areaCreator = new AreaCreator();
	areaCreator.doFile("/home/pawel/Java/Entwürfe_und_Daten/Areas/Geodaten/dwd-Warngebiete_Binnenseen.json");
	areaCreator.doFile("/home/pawel/Java/Entwürfe_und_Daten/Areas/Geodaten/dwd-Warngebiete_Bundeslaender.json");
	areaCreator.doFile("/home/pawel/Java/Entwürfe_und_Daten/Areas/Geodaten/dwd-Warngebiete_Gemeinden.json");
	areaCreator.doFile("/home/pawel/Java/Entwürfe_und_Daten/Areas/Geodaten/dwd-Warngebiete_Kreise.json");
	areaCreator.doFile("/home/pawel/Java/Entwürfe_und_Daten/Areas/Geodaten/dwd-Warngebiete_Kueste.json");
	areaCreator.doFile("/home/pawel/Java/Entwürfe_und_Daten/Areas/Geodaten/dwd-Warngebiete_See.json");
	areaCreator.flushToDisk("/home/pawel/Java/Entwürfe_und_Daten/Areas/Geodaten/areas.txt");
    }

    public static class Area{
        public String warncellID;
        public String warncenter;
        public int type;
        public String name;
        public ArrayList<String> polygonStrings;
    }

    public static class AreaCreator{

        final static String AREAHEADER = "Geodata version: 2, 12.06.2021";
        final static String ITEMSEPERATOR = "@";

        ArrayList<Area> areaArrayList;

        public AreaCreator(){
            System.out.println("Hallo!");
            areaArrayList = new ArrayList<Area>();
        }

        public void doFile(String filename){
            File file = new File(filename);
            System.out.println("Processing: "+filename);
            try {
                InputStream inputStream = new FileInputStream(file);
                Gson gson = new Gson();
                JsonReader jsonReader = new Gson().newJsonReader(new FileReader(file));
                AreaDataClass areaDataClass = gson.fromJson(jsonReader,AreaDataClass.class);
                //System.out.println(areaDataClass.type);
                //System.out.println(areaDataClass.totalFeatures);
                //System.out.println("Size: "+areaDataClass.features.length);
                for (int i=0; i<areaDataClass.features.length; i++){
                    areaDataClass.features[i].polygonRawStrings = new ArrayList<String>();
                    System.out.println("-------------------------------------------------------");
                    System.out.println("Name: "+areaDataClass.features[i].properties.NAME);
                    //System.out.println    ("Coordinates-Elements : "+areaDataClass.features[i].geometry.coordinates.size());
                    JsonArray j = areaDataClass.features[i].geometry.coordinates;
                    for (int k=0; k<areaDataClass.features[i].geometry.coordinates.size(); k++){
                        JsonElement element2 = j.get(k);
                        JsonArray subarray = element2.getAsJsonArray();
                        //System.out.println("     Subarray "+k+"      : "+subarray.size());
                        boolean written = false;
                        for (int pos=0; pos<subarray.size(); pos++){
                            JsonElement posElement = subarray.get(pos);
                            JsonArray posArray = posElement.getAsJsonArray();
                            //System.out.println(" ..> ElementArray "+pos+" has "+ posArray.size()+" pairs");
                            // We are almost there. Each entry now consists of the two values
                            boolean pairsInPosArray=false;
                            String polygonRawString="";
                            for (int valuecounter=0; valuecounter<posArray.size(); valuecounter=valuecounter+1){
                                JsonElement l = posArray.get(valuecounter);
                                //System.out.println(l.toString());
                                if (l.isJsonArray()){
                                    JsonArray pairArray = l.getAsJsonArray();
                                    float lat = pairArray.get(0).getAsFloat();
                                    float lon = pairArray.get(1).getAsFloat();
                                    //System.out.println(lat+";"+lon);
                                    //polygonRawString=polygonRawString+lat+","+lon;
                                    polygonRawString=polygonRawString+lon+","+lat;
                                    if (valuecounter<posArray.size()-1){
                                        polygonRawString=polygonRawString+" ";
                                    }
                                } else {
                                    pairsInPosArray=true;
                                    System.out.println("break");
                                    break;
                                }
                            }
                            if ((pairsInPosArray) && (!written)){
                                System.out.println("PPA");
                                //System.out.println("posArray size : "+posArray.size());
                                // im subarray sind die Wertpaare
                                for (int valuecounter=0; valuecounter< subarray.size(); valuecounter++){
                                    JsonElement l = subarray.get(valuecounter);
                                    //System.out.println(l.toString());
                                    JsonArray pairArray = l.getAsJsonArray();
                                    float lat = pairArray.get(0).getAsFloat();
                                    float lon = pairArray.get(1).getAsFloat();
                                    //System.out.println(lat+";"+lon);
                                    //polygonRawString=polygonRawString+lat+","+lon;
                                    polygonRawString=polygonRawString+lon+","+lat;
                                    if (valuecounter<subarray.size()-1){
                                        polygonRawString=polygonRawString+" ";
                                    }
                                }
                                written = true;
                            }
                            // add to arraylist
                            if (!polygonRawString.equals("")){
                                areaDataClass.features[i].polygonRawStrings.add(polygonRawString);
                            }
                            // System.out.println("Polygon-Raw:" + polygonRawString);
                            // System.out.println("polygons included:" + areaDataClass.features[i].polygonRawStrings.size());
                        }
                    }
                    // flush to Arraylist
                    Area area = new Area();
                    area.name = areaDataClass.features[i].properties.NAME;
                    area.type = areaDataClass.features[i].properties.TYPE;
                    area.polygonStrings = areaDataClass.features[i].polygonRawStrings;
                    area.warncellID = areaDataClass.features[i].properties.WARNCELLID;
                    area.warncenter = areaDataClass.features[i].properties.WARNCENTER;
                    areaArrayList.add(area);

                }
            } catch (Exception e){
                System.out.println("Error reading area file: "+e.getMessage()+"|"+e.getCause());
            }

        }

        private final static String serial_serparator = "_,_";

        private String serializeString(ArrayList<String> s) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i=0; i<s.size(); i++){
                stringBuilder.append(s.get(i));
                if (i<s.size()-1){
                    stringBuilder.append(serial_serparator);
                }
            }
            return stringBuilder.toString();
        }

        public void flushToDisk(String filename){
            File file = new File(filename);
            System.out.println("Writing...");
            try {
                //FileOutputStream fileOutputStream = new FileOutputStream(file);
                PrintWriter printWriter = new PrintWriter(filename);
                System.out.println(AREAHEADER);          
                printWriter.println(AREAHEADER);
                // 208439000,207,2,"Bodensee West",9.02076,47.728969,
                for (int i=0; i<areaArrayList.size(); i++){
                    StringBuilder stringBuilder = new StringBuilder();
                    String polygons = serializeString(areaArrayList.get(i).polygonStrings);
                    stringBuilder.append(areaArrayList.get(i).warncellID);
                    stringBuilder.append(ITEMSEPERATOR);
                    stringBuilder.append(areaArrayList.get(i).warncenter);
                    stringBuilder.append(ITEMSEPERATOR);
                    stringBuilder.append(areaArrayList.get(i).type);
                    stringBuilder.append(ITEMSEPERATOR);
                    stringBuilder.append("\"");
                    stringBuilder.append(areaArrayList.get(i).name);
                    stringBuilder.append("\"");
                    stringBuilder.append(ITEMSEPERATOR);
                    stringBuilder.append(polygons);
                    String result = stringBuilder.toString();
                    printWriter.println(result);
                    //System.out.println(result);
                }
                printWriter.close();
            } catch (Exception e){
                System.out.println("Error writing data: "+e.getMessage()+"|"+e.getCause());
            }
        }

    }


    public static class Geometry{
        String type;
        //ArrayList<ArrayList<ArrayList<wertepaare>>> coordinates;
          //List<List<List<List<Double>>>> coordinates;
        //List<List<List<Double[]>>>                              coordinates;
        //double[][][][] coordinates;
        JsonArray coordinates;
    }

    public static class Feature{
        String type;
        String id;
        String geomentry_name;
        Geometry geometry;
        Properties properties;
        ArrayList<String> polygonRawStrings;
    }

    public static class AreaDataClass{
        String type;
        String totalFeatures;
        Feature[] features;
    }

    public static class Properties{
        String WARNCELLID;
        String WARNCENTER;
        String NAME;
        int TYPE;
    }
}
