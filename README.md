# Area-Generator

This tiny java program generates the areas.txt file from json data

The json files (see source) can be obtained from the GeoServer of the Deutscher Wetterdienst (DWD)